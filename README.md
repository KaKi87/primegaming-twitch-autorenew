# Twitch Prime subscription auto-renew

Automatically renew your Twitch Prime subscription to your favourite channel.

Requirements : [Git](https://git-scm.com/download), [NodeJS](https://nodejs.org/en/download/), [Yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable)

Install : `git clone https://git.kaki87.net/KaKi87/twitch-prime-auto-renew.git && cd twitch-prime-auto-renew && yarn install`

Run : `USERNAME="your_username" PASSWORD="your_password" TOTP="totp_key" CHANNEL="your_favourite_channel" node index.js`

*Do not specify `TOTP="totp_key"` if you're not using two-factor authentication (2FA), not recommended.*
const
    {
        USERNAME,
        PASSWORD,
        TOTP,
        CHANNEL
    } = process.env,
    puppeteer = require('puppeteer-extra'),
    stealth = require('puppeteer-extra-plugin-stealth'),
    totp = require('totp-generator'),
    root = 'https://www.twitch.tv',
    usernameInputSelector = '#login-username',
    passwordInputSelector = '#password-input',
    loginButtonSelector = 'button[data-a-target="passport-login-button"]',
    otpInputSelector = 'input[autocomplete="one-time-code"]',
    otpButtonSelector = 'button[target="submit_button"]',
    followingLinkSelector = 'a[data-a-target="following-link"]',
    accountCheckupCancelButtonSelector = '[data-a-target="account-checkup-generic-modal-secondary-button"]',
    subscribeButtonSelector = 'button[data-a-target="subscribe-button"]',
    primeSubscribeCheckboxSelector = '.tw-checkbox__label',
    primeSubscribeButtonSelector = 'button[state="default"]',
    subscribeModalSelector = '.sub-modal';

puppeteer.use(stealth());

(async () => {
    const
        browser = await puppeteer.launch({ headless: false }),
        page = await browser.newPage();
    await page.goto(`${root}/login`);
    await (await page.waitForSelector(usernameInputSelector)).click();
    await page.keyboard.type(USERNAME);
    await (await page.waitForSelector(passwordInputSelector)).click();
    await page.keyboard.type(PASSWORD);
    await (await page.waitForSelector(loginButtonSelector)).click();
    if(TOTP){
        await (await page.waitForSelector(otpInputSelector)).click();
        await page.keyboard.type(totp(TOTP));
        await (await page.waitForSelector(otpButtonSelector)).click();
    }
    await page.waitForSelector(followingLinkSelector);
    await page.goto(`${root}/${CHANNEL}`);
    try { await (await page.waitForSelector(accountCheckupCancelButtonSelector)).click(); } catch {}
    await (await page.waitForSelector(subscribeButtonSelector)).click();
    await (await page.waitForSelector(primeSubscribeCheckboxSelector)).click();
    await (await page.waitForSelector(primeSubscribeButtonSelector)).click();
    await page.waitForSelector(subscribeModalSelector, { hidden: true });
    await browser.close();
})();